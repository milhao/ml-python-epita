# Benvindo ao Aprendisado de Maquina com Python Parte I! 

você vai encontrar neste repositorio alguns material para o courso. 

Para beneficiar bastamte do codigo, você vai precisar instalar o docker e configurar e executar a imagem configurada. isto vai garantir que todas as dependencias são instaladas quando voce usar o código e as sessões JupyterLab. 

If you have any question about the course, feel free to reach out via mail or within gitlab. 

## Execute o docker e use o jupyter laboratorio. 
It's pretty simple. 
From the folder `Conf`, run in command line:
`sudo ./build.sh`, it will build (if not already done) and run a configured container with Jupyter Lab that you can reach locally in you favorite navigator. 

In the command line window where you ran `sudo ./build.sh`, a URL of the format `http://127.0.0.1:8888/?token=41ed316c2c1ac462` will be displayed, copy paste it in your URL, and change the port from `8888` to `32788`. Jupyter Lab should be up and running. 

In `/app/Repo/ml-python-epita` you'll find the materials of the course. 


